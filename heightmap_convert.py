import sys
import math
from pathlib import Path
from nbt.world import AnvilWorldFolder
import png

import traceback

defaultPalette = [
	[0,		87, 0, 101],	# Bedrock layer
	[63,	0, 26, 48],	# Bottom of the (old gen) world, I'm assuming no ocean floor will be deeper
	[126,	0, 141, 255],	# Sea level (water)
	[127,	25, 109, 25],	# Sea level (land) (alt 63)
	[144,	55, 237, 19],	# Lowland average (alt 80)
	[164,	255, 255, 63],	# Highland transition (alt 100)
	[194,	134, 0, 0],	# Mountain transition (alt 130)
	[244,	215, 147, 147],	# High mountain transition (alt 180)
	[320,	255, 255, 255],	# Top of the natural world generation (alt 256)
	[384,	225, 109, 194]	# Top of the world (alt 320)
]
greyscalePalette = [
	[0,		0, 0, 0],	# Bedrock layer
	[126,	96, 96, 96],	# Sea level (water)
	[127,	127, 127, 127],	# Sea level (land)
	[384,	255, 255, 255]	# Top of the world (alt 320)
]

palettes = {"DEFAULT":defaultPalette, "GREYSCALE":greyscalePalette}

def lerp(a, b, c): return (c*a) + ((1-c)*b)
def convert_color(greyscale, palette):
	for i in range(len(palette)):
		upperThreshold = palette[i][0];
		if greyscale == upperThreshold:
			return palette[i][1:]
		elif greyscale < upperThreshold:
			lowerThreshold = palette[i-1][0]
			coeff = (greyscale - lowerThreshold) / (upperThreshold - lowerThreshold)
			return [math.floor(lerp(palette[i][1], palette[i-1][1], coeff)),
					math.floor(lerp(palette[i][2], palette[i-1][2], coeff)),
					math.floor(lerp(palette[i][3], palette[i-1][3], coeff))]
	return [0,0,0]

def to_heightmap_1d(long_array):
	result = []
	iterations = 0
	for long in long_array:
		iterations += 1
		result.append(long % 512)
		result.append((long >> 9) % 512)
		result.append((long >> 18) % 512)
		result.append((long >> 27) % 512)
		if(iterations > 37):
			break
		elif(iterations < 37):
			result.append((long >> 36) % 512)
			result.append((long >> 45) % 512)
			result.append((long >> 54) % 512)
	return result

def to_heightmap(long_array):
	heightmap = to_heightmap_1d(long_array)
	
	result = [ [] ]
	i = 0
	
	for height in heightmap:
		result[-1].append(height)
		i += 1
		if i > 15:
			result.append([])
			i = 0
	result.pop()
	return result


def chunk_to_heightmap(chunk, exportToFiles=False, heightmapsToConvert = 0b1111):
	renderWS = bool(heightmapsToConvert & 0b0001)
	renderOF = bool(heightmapsToConvert & 0b0010)
	renderMB = bool(heightmapsToConvert & 0b0100)
	renderNL = bool(heightmapsToConvert & 0b1000)
	
	if renderWS:
		hWorldSurface = to_heightmap(chunk.get('Heightmaps').get('WORLD_SURFACE'))
	if renderOF:
		hOceanFloor = to_heightmap(chunk.get('Heightmaps').get('OCEAN_FLOOR'))
	if renderMB:
		hMotionBlocking = to_heightmap(chunk.get('Heightmaps').get('MOTION_BLOCKING'))
	if renderNL:
		hMotionBlockingNL = to_heightmap(chunk.get('Heightmaps').get('MOTION_BLOCKING_NO_LEAVES'))
	
	if exportToFiles:
		filename = chunk.get('xPos').valuestr() + '_' + chunk.get('zPos').valuestr() + '.png'
		writer = png.Writer(16, 16, greyscale=True, bitdepth=16)
		
		if renderWS:
			with open('./output/world_surface/' + filename, 'wb') as f:
				writer.write(f, hWorldSurface)
		if renderOF:
			with open('./output/ocean_floor/' + filename, 'wb') as f:
				writer.write(f, hOceanFloor)
		if renderMB:
			with open('./output/motion_blocking/' + filename, 'wb') as f:
				writer.write(f, hMotionBlocking)
		if renderNL:
			with open('./output/motion_blocking_no_leaves/' + filename, 'wb') as f:
				writer.write(f, hMotionBlockingNL)
	else:
		result = {}
		if renderWS: result["WS"] = hWorldSurface;
		if renderOF: result["OF"] = hOceanFloor;
		if renderMB: result["MB"] = hMotionBlocking;
		if renderNL: result["NL"] = hMotionBlockingNL;
		return result


def region_to_heightmap16(region, heightmapsToRender, silent=False):
	renderWS = bool(heightmapsToRender & 0b0001)
	renderOF = bool(heightmapsToRender & 0b0010)
	renderMB = bool(heightmapsToRender & 0b0100)
	renderNL = bool(heightmapsToRender & 0b1000)
	
	filename = str(region.loc.x) + "_" + str(region.loc.z) + ".png"
	if not silent:
		print("Generating heightmap for region " + str((region.loc.x, region.loc.z)))
	
	hWorldSurface = []
	hOceanFloor = []
	hMotionBlocking = []
	hMotionBlockingNL = []
	tempList = [0]*512
	if renderWS:
		for n in range(512):
			hWorldSurface.append(tempList[:])
	if renderOF:
		for n in range(512):
			hOceanFloor.append(tempList[:])
	if renderMB:
		for n in range(512):
			hMotionBlocking.append(tempList[:])
	if renderNL:
		for n in range(512):
			hMotionBlockingNL.append(tempList[:])
	
	for x in range(32):
		for y in range(32):
			try:
				chunk = region.get_nbt(x,y)
				chunkHM = chunk_to_heightmap(chunk, False, heightmapsToRender)
				
				for i in range(16):
					xDef = (x * 16) + i
					for j in range(16):
						yDef = (y * 16) + j
						if renderWS: hWorldSurface[yDef][xDef] = chunkHM["WS"][j][i]
						if renderOF: hOceanFloor[yDef][xDef] = chunkHM["OF"][j][i]
						if renderMB: hMotionBlocking[yDef][xDef] = chunkHM["MB"][j][i]
						if renderNL: hMotionBlockingNL[yDef][xDef] = chunkHM["NL"][j][i]
			except:
				if not silent:
					print('An exception occured while trying to load chunk ' + str(x) + ',' + str(y) + ' in region ' + str(region) + ' : ' + str(sys.exc_info()))
					
	writer = png.Writer(512, 512, greyscale=True, bitdepth=16)
	
	if renderWS:
		with open('./output/world_surface/' + filename, 'wb') as f:
			writer.write(f, hWorldSurface)
	if renderOF:
		with open('./output/ocean_floor/' + filename, 'wb') as f:
			writer.write(f, hOceanFloor)
	if renderMB:
		with open('./output/motion_blocking/' + filename, 'wb') as f:
			writer.write(f, hMotionBlocking)
	if renderNL:
		with open('./output/motion_blocking_no_leaves/' + filename, 'wb') as f:
			writer.write(f, hMotionBlockingNL)

def region_to_heightmap(region, heightmapsToRender, palette, silent=False):
	renderWS = bool(heightmapsToRender & 0b0001)
	renderOF = bool(heightmapsToRender & 0b0010)
	renderMB = bool(heightmapsToRender & 0b0100)
	renderNL = bool(heightmapsToRender & 0b1000)
	
	filename = str(region.loc.x) + "_" + str(region.loc.z) + ".png"
	if not silent:
		print("Generating heightmap for region " + str((region.loc.x, region.loc.z)))
	
	hWorldSurface = []
	hOceanFloor = []
	hMotionBlocking = []
	hMotionBlockingNL = []
	tempList = [0]*1536
	if renderWS:
		for n in range(512):
			hWorldSurface.append(tempList[:])
	if renderOF:
		for n in range(512):
			hOceanFloor.append(tempList[:])
	if renderMB:
		for n in range(512):
			hMotionBlocking.append(tempList[:])
	if renderNL:
		for n in range(512):
			hMotionBlockingNL.append(tempList[:])
	
	for x in range(32):
		for y in range(32):
			try:
				chunk = region.get_nbt(x,y)
				chunkHM = chunk_to_heightmap(chunk, False, heightmapsToRender)
				
				for j in range(16):
					yDef = (y * 16) + j
					for i in range(16):
						xDef = (x * 48) + (i * 3)
						if renderWS:
							color = convert_color(chunkHM["WS"][j][i], palette)
							hWorldSurface[yDef][xDef] = color[0]
							hWorldSurface[yDef][xDef+1] = color[1]
							hWorldSurface[yDef][xDef+2] = color[2]
						if renderOF:
							color = convert_color(chunkHM["OF"][j][i], palette)
							hOceanFloor[yDef][xDef] = color[0]
							hOceanFloor[yDef][xDef+1] = color[1]
							hOceanFloor[yDef][xDef+2] = color[2]
						if renderMB:
							color = convert_color(chunkHM["MB"][j][i], palette)
							hMotionBlocking[yDef][xDef] = color[0]
							hMotionBlocking[yDef][xDef+1] = color[1]
							hMotionBlocking[yDef][xDef+2] = color[2]
						if renderNL:
							color = convert_color(chunkHM["NL"][j][i], palette)
							hMotionBlockingNL[yDef][xDef] = color[0]
							hMotionBlockingNL[yDef][xDef+1] = color[1]
							hMotionBlockingNL[yDef][xDef+2] = color[2]
			except:
				if not silent:
					print('An exception occured while trying to load chunk ' + str(x) + ',' + str(y) + ' in region ' + str(region) + ' : ')
					traceback.print_exc()
	
	writer = png.Writer(512, 512, greyscale=False, bitdepth=8)
	
	if renderWS:
		with open('./output/world_surface/' + filename, 'wb') as f:
			writer.write(f, hWorldSurface)
	if renderOF:
		with open('./output/ocean_floor/' + filename, 'wb') as f:
			writer.write(f, hOceanFloor)
	if renderMB:
		with open('./output/motion_blocking/' + filename, 'wb') as f:
			writer.write(f, hMotionBlocking)
	if renderNL:
		with open('./output/motion_blocking_no_leaves/' + filename, 'wb') as f:
			writer.write(f, hMotionBlockingNL)


# Rendu de la carte complète au format PNG RGB 8-bit (avec une palette)
def main(worldFolder, heightmapsToRender = 0b1111, bounds = None, palette = defaultPalette):
	Path("./output").mkdir(exist_ok=True)
	if heightmapsToRender & 0b0001: Path("./output/world_surface").mkdir(exist_ok=True)
	if heightmapsToRender & 0b0010: Path("./output/ocean_floor").mkdir(exist_ok=True)
	if heightmapsToRender & 0b0100: Path("./output/motion_blocking").mkdir(exist_ok=True)
	if heightmapsToRender & 0b1000: Path("./output/motion_blocking_no_leaves").mkdir(exist_ok=True)
	
	wf = AnvilWorldFolder(worldFolder)
	if wf.nonempty():
		if bounds == None:
			for region in wf.iter_regions():
				try:
					region_to_heightmap(region, heightmapsToRender, palette)
				except:
					traceback.print_exc()
		else:
			for xReg in range(bounds[0], bounds[2] + 1, 1):
				for yReg in range(bounds[1], bounds[3] + 1, 1):
					try:
						region_to_heightmap(wf.get_region(xReg, yReg), heightmapsToRender, palette)
					except:
						traceback.print_exc()

# Rendu de la carte complète au format PNG L 16-bit (0-384 malgré le maximum de 65536)
def main16(worldFolder, heightmapsToRender = 0b1111, bounds = None):
	Path("./output").mkdir(exist_ok=True)
	if heightmapsToRender & 0b0001: Path("./output/world_surface").mkdir(exist_ok=True)
	if heightmapsToRender & 0b0010: Path("./output/ocean_floor").mkdir(exist_ok=True)
	if heightmapsToRender & 0b0100: Path("./output/motion_blocking").mkdir(exist_ok=True)
	if heightmapsToRender & 0b1000: Path("./output/motion_blocking_no_leaves").mkdir(exist_ok=True)
	
	wf = AnvilWorldFolder(worldFolder)
	if wf.nonempty():
		if bounds == None:
			for region in wf.iter_regions():
				try:
					region_to_heightmap16(region, heightmapsToRender)
				except:
					traceback.print_exc()
		else:
			for xReg in range(bounds[0], bounds[2] + 1, 1):
				for yReg in range(bounds[1], bounds[3] + 1, 1):
					try:
						region_to_heightmap16(wf.get_region(xReg, yReg), heightmapsToRender)
					except:
						traceback.print_exc()
	

def print_usage():
	print("""\
Usage : <file>.py "path/to/world/folder" [-ws -of -mb -nl -16b] [-b (x1,z1,x2,z2)]
	-ws, --world-surface : Render the WORLD_SURFACE heightmap
	-of, --ocean-floor : Render the OCEAN_FLOOR heightmap
	-mb, --motion-blocking : Render the MOTION_BLOCKING heightmap
	-nl, -mbnl, --no-leaves, --motion-blocking-no-leaves : Render the MOTION_BLOCKING_NO_LEAVES heightmap
	-16b, --16-bit : Render a greyscale, 16-bit PNG "raw" heightmap instead of a coloured one.
	-b, --bounds (xmin,zmin,xmax,zmax) : Specify the bounds of the render, in regions, all values inclusive : for instance, '-b (-1,0,1,2)' would render regions r.-1.0.mca to r.1.2.mca.
	""")

if __name__ == "__main__":
	if(len(sys.argv) < 2):
		print("Incorrect syntax.", end=" ")
		print_usage()
		sys.exit(5) # errno.EIO (I/O)
	
	heightmapsToRender = 0b0000
	gen16BitImages = False
	bounds = None
	
	forceContinue = False
	for i in range(2, len(sys.argv)):
		if forceContinue:
			forceContinue = False
			continue
		match sys.argv[i]:
			case "-ws" | "--world-surface":
				heightmapsToRender |= 0b0001
			case "-of" | "--ocean-floor":
				heightmapsToRender |= 0b0010
			case "-mb" | "--motion-blocking":
				heightmapsToRender |= 0b0100
			case "-mbnl" | "-nl" | "--motion-blocking-no-leaves" | "--no-leaves":
				heightmapsToRender |= 0b1000
			case "-16b" | "--16-bit":
				gen16BitImages = True
			case "-b" | "--bounds":
				if i + 1 >= len(sys.argv):
					print("No bounds specified after flag " + str(sys.argv[i]))
					print_usage()
					sys.exit(22) # errno.EINVAL (Invalid argument)
				else:
					bounds = eval(sys.argv[i+1])
					forceContinue = True
					if type(bounds) is not tuple or len(bounds) != 4:
						print("Bounds specified wrongly after flag " + str(sys.argv[i]))
						print_usage()
						sys.exit(22) # errno.EINVAL (Invalid argument)
			case _:
				print("Invalid flag " + str(sys.argv[i]))
				print_usage()
				sys.exit(22) # errno.EINVAL (Invalid argument)
	
	if heightmapsToRender == 0b0000:
		print("No images would be rendered ! Specify at least one of -ws, -of, -mb and -nl.")
		print_usage()
		sys.exit(22) # errno.EINVAL (Invalid argument)
	
	if gen16BitImages:
		main16(sys.argv[1], heightmapsToRender, bounds)
	else:
		main(sys.argv[1], heightmapsToRender, bounds)