# Minecraft Heightmap Exporter

![Example of an OCEAN_FLOOR heightmap with a color palette](doc/images/of_coloured.png)
![Example of a MOTION_BLOCKING_NO_LEAVES heightmap in greyscale](doc/images/nl_greyscale.png)

This program extracts the heightmaps from Minecraft regions, and exports them to PNG (either 8-bit or 16-bit). Compatible only with **Minecraft 1.18.2+** worlds.


## Getting started

### Requirements
- [Python](https://www.python.org/downloads/) v3.10
- [PyPNG](https://github.com/drj11/pypng) (`pip install pypng`)
- [NBT](https://github.com/twoolie/NBT) (`pip install NBT`)

### Installation
1. Make sure you have installed all the required software/packages listed above.
2. Clone or download this repository anywhere you like.

## Usage

### Basic
The syntax of the command is :
`py heightmap_convert.py "path/to/world/folder" [-ws -of -mb -nl -16b] [-b (x1,z1,x2,z2)]`

The following flags and arguments may be provided :
- **-ws** or **--world-surface** : enable rendering the WORLD_SURFACE heightmap
- **-of** or **--ocean-floor** : enable rendering the OCEAN_FLOOR heightmap
- **-mb** or **--motion-blocking** : enable rendering the MOTION_BLOCKING heightmap
- **-nl**, **-mbnl**, **no-leaves** or **--motion-blocking-no-leaves** : enable rendering the MOTION_BLOCKING_NO_LEAVES heightmap
- **-16b** or **--16-bit** : render 16-bit greyscale PNGs instead of 8-bit coloured ones
- **-b (x1,z1,x2,z2)** or **--bounds (x1,z1,x2,z2)** : render only regions inside the specified bounding box (from (x1,z1) to (x2,z2), all values inclusive)

The *"path/to/world/folder"* should not point to the */region* directory, only its parent (where the file *level.dat* is found).

### Minecraft heightmaps
Minecraft stores four different heightmaps in each region file, each of whose has its specificities:
- **WORLD_SURFACE** is the highest *non-air block* in each column, even if non-solid (like water or grass)
- **OCEAN_FLOOR** is the highest *solid block* in each column
- **MOTION_BLOCKING** is the same as OCEAN_FLOOR, except it also matches fluids
- **MOTION_BLOCKING_NO_LEAVES** is the same as MOTION_BLOCKING, except it doesn't match blocks in the *#minecraft:leaves* block tag

More information about region files and heightmaps can be found [on the Minecraft Wiki](https://minecraft.fandom.com/wiki/Chunk_format).

### Examples
To export (in 8-bit RGB PNG) the OCEAN_FLOOR heightmap of the entire "test" world :
```py heightmap_convert.py test -of```

To export (in 8-bit RGB PNG) the WORLD_SURFACE and OCEAN_FLOOR heightmaps of regions (2,-3) to (6,5) of the world "default2", located in the default Minecraft directory (on Windows) :
```py heightmap_convert.py "C:/Users/<username>/AppData/Roaming/.minecraft/saves/default2" -ws -of -b```

Same example as above, on Linux (untested) :
```py heightmap_convert.py "~/.minecraft/saves/default2" -ws -of -b```

To export a 16-bit greyscale PNG of the MOTION_BLOCKING heightmap for region (47,82) of the world "saves/superflat" :
```py heightmap_convert.py "saves/superflat" -mb -16b -b (47,82,47,82)```

### Advanced usage
If you import *heightmap_convert.py* into your own project, you can make use of the following functions :
- `main(worldFolder, heightmapsToRender, bounds)` and `main16(worldFolder, heightmapsToRender, bounds)` yield the same result as calling the script ; it exports either 8-bit coloured (*main*) or 16-bit greyscale (*main16*) maps for the world at path *worldFolder*.
  - `bounds` should be `None` if you want to render the whole world, and a 4-tuple (x1,z1,x2,z2) otherwise
  - `heightmapsToRender` is an integer from 0b0000 to 0b1111 ; each bit is 1 if you want to render the corresponding heightmap, and zero otherwise. From least to most significant, the bits are mapped to :
    1. WORLD_SURFACE
	2. OCEAN_FLOOR
	3. MOTION_BLOCKING
	4. MOTION_BLOCKING_NO_LEAVES
- `region_to_heightmap16(region, heightmapsToRender, silent=False)` exports a single 16-bit PNG image for each 1 bit in *heightmapsToRender* (see above for details about this variable).
  - `region` is a [NBT](https://github.com/twoolie/NBT) RegionFile
  - `silent`, if true, the function neither displays log messages ("Generating heightmap for region X,Z") nor exceptions catched in the chunk reading loop (99.9% of which are related to missing/non-generated chunks and can be safely ignored)
- `region_to_heightmap(region, heightmapsToRender, silent=False)` exports a single 8-bit PNG image for each 1 bit in *heightmapsToRender* (see above for details about this variable).
  - `region` is a [NBT](https://github.com/twoolie/NBT) region
  - `palette` is a color palette defined as a list of lists. Each sub-list contains 4 integers : the first one sets the altitude at which this color will be displayed, and the 3 following integers (0-255) encode the three color components (R, G, B). Examples of palettes can be found in the variable `palettes`.
  - `silent`, if true, the function neither displays log messages ("Generating heightmap for region X,Z") nor exceptions catched in the chunk reading loop (99.9% of which are related to missing/non-generated chunks and can be safely ignored)
- `chunk_to_heightmap(chunk, exportToFiles=False, heightmapsToConvert=0b1111)` converts a single chunk heightmap information to a list of heights, and either exports it to image if the `exportToFiles` flag is true (only 16-bit greyscale PNG is supported), or returns a list of lists containing all of the altitudes (0-384). For information about `heightmapsToConvert`, see `heightmapsToRender` above.
  - `chunk` is a [NBT](https://github.com/twoolie/NBT) NBTFile


## Planned features
- [ ] Write some documentation
- [ ] Export of GeoTIFF heightmaps
- [ ] Multiple color palettes
- [ ] User-defined color palettes
- [ ] Export of bathymetry maps (MOTION_BLOCKING-OCEAN_FLOOR when >0, transparent if =0) to visualize the rivers and lakes higher than sea level


## Licence
This program is distributed under the MIT licence, see the [LICENCE](LICENCE) for details.


## Contact
- @infomase ([Twitter](https://twitter.com/infomase))
<!-- Project link : https://gitlab.com/infomase/TODO -->
