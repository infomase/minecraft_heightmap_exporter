## [0.1]	(Current)	–	2022-08-08
### Modified :
 - Corrected the *README.md* "Planned features" task list.

## [Initial commit]	(66087be6)	–	2022-08-08
### Added :
 - The file **heightmap_convert.py** contains functions which are able to convert a Minecraft 1.18 world (or any rectangle of regions from this world) to .PNG heightmaps, either in 8-bit RGB or in 16-bit grayscale. Call the script without any arguments to display usage (*py heightmap_convert.py*).
   - It is not yet possible to change the 8-bit color palette except by editing the script or importing it and calling the right functions manually.
   - The PNG heightmaps are exported in the */output* directory, in a specific subdirectory depending on the heightmap type (either */world_surface*, */ocean_floor*, */motion_blocking* or */motion_blocking_no_leaves*).
 - A standard .gitignore file has been copied from https://github.com/github/gitignore/blob/main/Python.gitignore ; a single filter was added in the beginning to ignore the */output* directory.
 - 2 images have been added in */doc/images* to decorate the readme.
